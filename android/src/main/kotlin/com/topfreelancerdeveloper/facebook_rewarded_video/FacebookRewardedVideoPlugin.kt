package com.topfreelancerdeveloper.facebook_rewarded_video

import android.app.Activity
import androidx.annotation.NonNull;
import com.facebook.ads.AdSettings
import com.facebook.ads.AudienceNetworkAds

import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry.Registrar

/** FacebookRewardedVideoPlugin */
public class FacebookRewardedVideoPlugin: FlutterPlugin, MethodCallHandler,ActivityAware {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private lateinit var channel : MethodChannel
  private lateinit var activity: Activity
  private var ads = HashMap<String,FANRewardedAdWrapper>()

  override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
    channel = MethodChannel(flutterPluginBinding.getFlutterEngine().getDartExecutor(), "facebook_rewarded_video")
    channel.setMethodCallHandler(this);
  }

  // This static function is optional and equivalent to onAttachedToEngine. It supports the old
  // pre-Flutter-1.12 Android projects. You are encouraged to continue supporting
  // plugin registration via this function while apps migrate to use the new Android APIs
  // post-flutter-1.12 via https://flutter.dev/go/android-project-migration.
  //
  // It is encouraged to share logic between onAttachedToEngine and registerWith to keep
  // them functionally equivalent. Only one of onAttachedToEngine or registerWith will be called
  // depending on the user's project. onAttachedToEngine or registerWith must both be defined
  // in the same class.
  companion object {
    @JvmStatic
    fun registerWith(registrar: Registrar) {
      val channel = MethodChannel(registrar.messenger(), "facebook_rewarded_video")
      val activity = registrar.activity()
      channel.setMethodCallHandler(FacebookRewardedVideoPlugin())
    }
  }

  override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
    if (call.method == "init") {
      var testDevices : List<*>? = (call.arguments as? HashMap<*, *>)?.get("testDevices") as? List<*>?

      AudienceNetworkAds.initialize(activity)
      if (testDevices != null)  {
        for (testDevice in testDevices) {
          if(testDevice != null)
          AdSettings.addTestDevice(testDevice as String)
        }
      }
      result.success(true)
      return
    }
    if (call.method == "create") {

      var adUnitId : String? = (call.arguments as? HashMap<*, *>)?.get("adUnitId") as? String?
      var forceCreate: Boolean? = (call.arguments as? HashMap<*, *>)?.get("forceCreate") as? Boolean?
      if (adUnitId == null || adUnitId?.length == 0){

        result.error("-1" , "empty or null adunitid" , null)
        return
      }
      if (forceCreate == null || forceCreate == false) {
        if (ads.contains(adUnitId)){
          result.error("-2" , "ad unit already has created", null)
          return
        }
      }
      ads[adUnitId] = FANRewardedAdWrapper(activity,adUnitId,channel)
      result.success(true)
      return
    }
    if (call.method == "load") {
      var adUnitId : String? = (call.arguments as? HashMap<*, *>)?.get("adUnitId") as? String?
      if (adUnitId == null || adUnitId?.length == 0){

        result.error("-1" , "empty or null adunitid" , null)
        return
      }
      if(ads[adUnitId] == null) {
        result.error("-2" , "ad unit id has no wrapper" , null)
        return
      }
      ads[adUnitId]?.load(result)
      return
    }
    if (call.method == "show") {
      var adUnitId : String? = (call.arguments as? HashMap<*, *>)?.get("adUnitId") as? String?
      var userId : String? = (call.arguments as? HashMap<*, *>)?.get("userId") as? String?
      var customData : String? = (call.arguments as? HashMap<*, *>)?.get("customData") as? String?
      if (adUnitId == null || adUnitId?.length == 0){

        result.error("-1" , "empty or null adunitid" , null)
        return
      }
      if(ads[adUnitId] == null) {
        result.error("-2", "ad unit id has no wrapper", null)
        return
      }
      if(ads[adUnitId]?.isLoaded() == false){
        result.error("-3", "ad not loaded", null)
        return
      }
      ads[adUnitId]?.show(userId , customData)
      result.success(true)
      return
    }
  }

  override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
    channel.setMethodCallHandler(null)
  }

  override fun onDetachedFromActivity() {

  }

  override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {

  }

  override fun onAttachedToActivity(binding: ActivityPluginBinding) {
    activity = binding.activity
  }

  override fun onDetachedFromActivityForConfigChanges() {

  }
}
