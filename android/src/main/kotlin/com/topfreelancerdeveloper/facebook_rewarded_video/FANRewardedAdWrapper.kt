package com.topfreelancerdeveloper.facebook_rewarded_video

import android.app.Activity
import com.facebook.ads.*
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.Result

class FANRewardedAdWrapper(context : Activity , adUnitId : String, channel: MethodChannel) : S2SRewardedVideoAdListener {
    private var channel : MethodChannel = channel
    private var context : Activity = context
    private var adUnitId : String = adUnitId
    private var ad : RewardedVideoAd = RewardedVideoAd(context, adUnitId)
    private var loadResult : Result? = null

    init {
        ad.setAdListener(this)
    }

    fun show(userId : String?, customData : String?){
        ad.setRewardData(RewardData(userId , customData))
        if(isLoaded()) {
        ad.show()
    }
}

    fun load(result : Result){
        loadResult = result
        ad.loadAd()
    }
    fun isLoaded() : Boolean {
        return ad.isAdLoaded && !ad.isAdInvalidated
    }

    override fun onRewardedVideoClosed() {
        var args =  HashMap<String,Any>()
        args["adUnitId"] = adUnitId
        channel?.invokeMethod("Closed",  args)
    }

    override fun onRewardServerSuccess() {
        var args =  HashMap<String,Any>()
        args["adUnitId"] = adUnitId
        channel?.invokeMethod("ServerSuccess",  args)
    }

    override fun onAdClicked(p0: Ad?) {
        var args =  HashMap<String,Any>()
        args["adUnitId"] = adUnitId
        channel?.invokeMethod("Clicked",  args)
    }

    override fun onRewardedVideoCompleted() {
        var args =  HashMap<String,Any>()
        args["adUnitId"] = adUnitId
        args["type"] = "facebook desnt have this"
        args["amount"] = 0
        channel?.invokeMethod("Rewarded",  args)
    }

    override fun onRewardServerFailed() {
        var args =  HashMap<String,Any>()
        args["adUnitId"] = adUnitId
        channel?.invokeMethod("ServerFail",  args)
    }

    override fun onError(p0: Ad?, p1: AdError?) {
        if(loadResult != null) {
            loadResult?.error(p1?.errorCode?.toString(), p1?.errorMessage?.toString(), null)
        }
        loadResult = null
    }

    override fun onAdLoaded(p0: Ad?) {
        if (loadResult != null) {
            loadResult?.success(true)
        }
        loadResult = null
    }

    override fun onLoggingImpression(p0: Ad?) {
    }
}