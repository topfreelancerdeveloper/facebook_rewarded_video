import Flutter
import UIKit
import FBAudienceNetwork

public class SwiftFacebookRewardedVideoPlugin: NSObject, FlutterPlugin {
    var ads = [String : FANRewardedAdWrapper]()
        private static var channel = FlutterMethodChannel()

  public static func register(with registrar: FlutterPluginRegistrar) {
    channel = FlutterMethodChannel(name: "facebook_rewarded_video", binaryMessenger: registrar.messenger())
    let instance = SwiftFacebookRewardedVideoPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    if call.method == "init" {
            let testDevices: [String]? = (call.arguments as? [String: Any])?["testDevices"] as? [String]
            //inin
            if testDevices != nil {
                //testdevcie
                FBAdSettings.setLogLevel(.debug)
                FBAdSettings.addTestDevices(testDevices!)
            }
            result(true)
            return
        }
    if call.method == "create" {
            let adUnitId: String? = (call.arguments as? [String: Any])?["adUnitId"] as? String
            let forceCreate: Bool? = (call.arguments as? [String: Any])?["forceCreate"] as?Bool
            if adUnitId == nil || adUnitId?.count == 0{
                result(FlutterError(code: "-1",
                    message: "empty or null adunitid",
                    details: nil)
                )
                return
            }
            if forceCreate == nil || forceCreate == false {
            if ads.contains(where: { (key: String, value: FANRewardedAdWrapper) -> Bool in
                key == adUnitId
            }){
               result(FlutterError(code: "-2",
                    message: "ad unit already has created",
                    details: nil)
                )
                return
            }
            }
        ads[adUnitId ?? ""] = FANRewardedAdWrapper(placementId: adUnitId!, channel: SwiftFacebookRewardedVideoPlugin.channel)
            result(true)
            return
        }
    if call.method == "load" {
            let adUnitId: String? = (call.arguments as? [String: Any])?["adUnitId"] as? String
            if adUnitId == nil || adUnitId?.count == 0{
                result(FlutterError(code: "-1",
                    message: "empty or null adunitid",
                    details: nil)
                )
                return
            }
            
            if ads[adUnitId ?? ""] == nil {
                result(FlutterError(code: "-2",
                    message: "ad unit id has no wrapper",
                    details: nil)
                )
                return
            }
            
            ads[adUnitId ?? ""]?.load(result: result)
            return
        }
    if call.method == "show" {
            let adUnitId: String? = (call.arguments as? [String: Any])?["adUnitId"] as? String
        let userId: String? = (call.arguments as? [String: Any])?["userId"] as? String
        let customData: String? = (call.arguments as? [String: Any])?["customData"] as? String
            if adUnitId == nil || adUnitId?.count == 0{
                result(FlutterError(code: "-1",
                    message: "empty or null adunitid",
                    details: nil)
                )
                return
            }
            
            if ads[adUnitId ?? ""] == nil {
                result(FlutterError(code: "-2",
                    message: "ad unit id has no wrapper",
                    details: nil)
                )
                return
            }
        
        ads[adUnitId ?? ""]?.setServerSideDate(userId: userId, customDate: customData)
            
            if (ads[adUnitId ?? ""]?.isReady() ?? false) == false{
                result(FlutterError(code: "-3",
                    message: "not loaded",
                    details: nil)
                )
                return
            }
            ads[adUnitId ?? ""]?.present(controller: (UIApplication.shared.delegate?.window??.rootViewController)!)
            result(true)
            return
        }
    result("iOS " + UIDevice.current.systemVersion)



  }
}
