import Foundation
import FBAudienceNetwork

class FANRewardedAdWrapper : NSObject, FBRewardedVideoAdDelegate {
    
    
    var channel : FlutterMethodChannel?
    var ad : FBRewardedVideoAd?
    var loadResult : FlutterResult?
        
    init(placementId : String , channel ch : FlutterMethodChannel) {
        super.init()
        ad = FBRewardedVideoAd.init(placementID: placementId)
        ad?.delegate = self
        channel = ch
    }
    
    func load(result : @escaping FlutterResult) {
        loadResult = result
        ad?.load()
    }
    
    func setServerSideDate(userId : String?, customDate : String?) {
        if userId == nil {
            return
        }
        if customDate != nil{
        ad?.setRewardDataWithUserID(userId!, withCurrency: customDate!)
            return
        }
        ad?.setRewardDataWithUserID(userId!, withCurrency: "")
    }
    
    func isReady() -> Bool? {
        return ad?.isAdValid ?? false
    }
    func present(controller : UIViewController) {
        if isReady() == false {
            return
        }
        ad?.show(fromRootViewController: controller)
    }
    
    func rewardedVideoAdDidLoad(_ rewardedVideoAd: FBRewardedVideoAd) {
        if loadResult != nil {
            loadResult!(true)
        }
        loadResult = nil
    }
    func rewardedVideoAd(_ rewardedVideoAd: FBRewardedVideoAd, didFailWithError error: Error) {
        if loadResult != nil {
            loadResult!(FlutterError(code: "-1",
                                     message: "load failed",
                                     details: error.localizedDescription
))
        }
        loadResult = nil
    }
    func rewardedVideoAdDidClick(_ rewardedVideoAd: FBRewardedVideoAd) {
        var args =  [String: Any]()
        args["adUnitId"] = rewardedVideoAd.placementID
              channel?.invokeMethod("Clicked", arguments: args)
    }
    func rewardedVideoAdDidClose(_ rewardedVideoAd: FBRewardedVideoAd) {
        var args =  [String: Any]()
        args["adUnitId"] = rewardedVideoAd.placementID
              channel?.invokeMethod("Closed", arguments: args)

    }
    
    func rewardedVideoAdVideoComplete(_ rewardedVideoAd: FBRewardedVideoAd) {
        var args =  [String: Any]()
        args["adUnitId"] = rewardedVideoAd.placementID
        args["type"] = "facebook doesnt have this"
        args["amount"] = 0
        channel?.invokeMethod("Rewarded", arguments: args)

    }
    func rewardedVideoAdServerRewardDidSucceed(_ rewardedVideoAd: FBRewardedVideoAd) {
        var args =  [String: Any]()
        args["adUnitId"] = rewardedVideoAd.placementID
              channel?.invokeMethod("ServerSuccess", arguments: args)
    }
    func rewardedVideoAdServerRewardDidFail(_ rewardedVideoAd: FBRewardedVideoAd) {
        var args =  [String: Any]()
        args["adUnitId"] = rewardedVideoAd.placementID
              channel?.invokeMethod("ServerFail", arguments: args)
    }
    
    
}
