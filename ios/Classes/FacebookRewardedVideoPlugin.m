#import "FacebookRewardedVideoPlugin.h"
#if __has_include(<facebook_rewarded_video/facebook_rewarded_video-Swift.h>)
#import <facebook_rewarded_video/facebook_rewarded_video-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "facebook_rewarded_video-Swift.h"
#endif

@implementation FacebookRewardedVideoPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftFacebookRewardedVideoPlugin registerWithRegistrar:registrar];
}
@end
