

import 'dart:async';
import 'dart:io';

import 'package:flutter/services.dart';

typedef LoadHandler = Function(dynamic error);
typedef EventHandler = Function(RewardedAdEvent event,
    {String type, double amount, String playError});

class FacebookRewardedVideo {
  static _RewardedAdManager manager = _RewardedAdManager(_channel);
  static final String testAdUnitId = Platform.isIOS
      ? 'ca-app-pub-3940256099942544/1712485313'
      : 'ca-app-pub-3940256099942544/5224354917';
  static const MethodChannel _channel =
      const MethodChannel('facebook_rewarded_video');

  static Future<bool> init({List<String> testDevices}) {
    return _channel.invokeMethod<bool>('init', {'testDevices': testDevices});
  }
}

class _RewardedAdManager {
  final MethodChannel _channel;
  final List<_RewardedAd> _ads = [];

  RewardedAdEvent _eventFromString(String call) {
    switch (call) {
      case "Rewarded":
        return RewardedAdEvent.rewarded;
      case "Opened":
        return RewardedAdEvent.opened;
      case "Closed":
        return RewardedAdEvent.closed;
      case "Clicked":
        return RewardedAdEvent.clicked;
      case "ServerSuccess":
        return RewardedAdEvent.ssv_success;
      case "ServerFailed":
        return RewardedAdEvent.ssv_failed;      
      case "PlaybackFailure":
        return RewardedAdEvent.playback_error;
      default:
        return null;
    }
  }

  _RewardedAdManager(this._channel) {
    _channel.setMethodCallHandler((call) async {
      String id = call.arguments['adUnitId'];
      try {
        _ads.where((element) => element.adUnitId == id).forEach((element) =>
            element.eventHandler?.call(_eventFromString(call.method),
                type: call.arguments['type'],
                amount: call.arguments['amount']?.toDouble(),
                playError: call.arguments['error']));
      } catch (err) {
        print(err);
      }
    });
  }

  Future<_RewardedAd> create(String adUnitId) async {
    int removeIndex =
        _ads.indexWhere((element) => element.adUnitId == adUnitId);
    if (removeIndex == -1) return _create(adUnitId, forceCreate: false);
    if (_ads[removeIndex].isObsolete) {
      _ads.removeAt(removeIndex);
      return _create(adUnitId, forceCreate: true);
    }
    return _ads[removeIndex];
  }

  Future<_RewardedAd> _create(String adUnitId, {bool forceCreate}) async {
    await _channel.invokeMethod(
        'create', {'adUnitId': adUnitId, 'forceCreate': forceCreate});
    _ads.add(_RewardedAd(adUnitId, _channel));
    return _ads.last;
  }
}

enum RewardedAdEvent { opened, closed, rewarded, playback_error,clicked,ssv_success,ssv_failed }

class _RewardedAd {
  final String adUnitId;
  bool _isReady = false;
  bool _isObsolete = false;
  EventHandler _eventHandler;
  EventHandler get eventHandler {
    return _eventHandler;
  }

  bool get isReady {
    return _isReady;
  }

  bool get isObsolete {
    return _isObsolete;
  }

  final MethodChannel _channel;

  _RewardedAd(this.adUnitId, this._channel);

  void load(LoadHandler handler) {
    assert(!isObsolete);
    if (!isReady)
      _channel.invokeMethod("load", {'adUnitId': adUnitId}).then((value) {
        _isReady = true;
        handler?.call(null);
      }).catchError((err) => handler?.call(err));
  }

  void show(EventHandler eventHandler, {String customData, String userId}) {
    assert(!isObsolete);
    if (eventHandler != null) _eventHandler = eventHandler;
    if (_isReady) {
      _isObsolete = true;
    }
    if (!_isReady) {
      return;
    }
    _channel.invokeMethod('show',
        {'adUnitId': adUnitId, 'customData': customData, 'userId': userId});
  }
}

